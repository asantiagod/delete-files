import name_former as nf

def test_name_from_number():
    assert nf.from_number(1234) == "DSC_1234.JPG"

def test_from_a_number_less_than_ten_thousand():
    assert nf.from_number(123) == "DSC_0123.JPG"
    assert nf.from_number(12) == "DSC_0012.JPG"
    assert nf.from_number(1) == "DSC_0001.JPG"

def test_bounds_of_number():
    assert nf.from_number(1) == "DSC_0001.JPG"
    assert nf.from_number(9999) == "DSC_9999.JPG"

def test_lower_than_lower_bound():
    assert nf.from_number(0) == ''

def test_greater_than_upper_bound():
    assert nf.from_number(10000) == ''

def test_name_list_from_number_list():
    numbers = [1,2,3,4]
    names = ["DSC_0001.JPG",
             "DSC_0002.JPG",
             "DSC_0003.JPG",
             "DSC_0004.JPG"]
    assert nf.from_list(numbers) == names

def test_names_list_from_numbers_file():
    FILE_NAME = 'numbers.txt'
    f = open(FILE_NAME,'w')
    f.writelines(map(lambda x:x+'\n',['1','2','3','4']))
    f.close()

    names = ["DSC_0001.JPG",
             "DSC_0002.JPG",
             "DSC_0003.JPG",
             "DSC_0004.JPG"]


    assert nf.from_file(FILE_NAME) == names

def test_names_file_from_numbers_file():
    INPUT_FILE = 'numbers.txt'
    OUTPUT_FILE = 'names.txt'
    wf = open(INPUT_FILE,'w')
    wf.writelines(map(lambda x:x+'\n',['1','2','3','4']))
    wf.close()

    names = ["DSC_0001.JPG",
             "DSC_0002.JPG",
             "DSC_0003.JPG",
             "DSC_0004.JPG"]

    nf.from_file(INPUT_FILE,OUTPUT_FILE)
    rf = open(OUTPUT_FILE)

    assert rf.read().splitlines() == names

