
def rename_extention(file_name ,input_extention, output_extention):
    return file_name.replace('.' + input_extention, 
                             '.' + output_extention)

ifile = open('names_to_delete.txt', 'r')
jpgFiles = ifile.read().splitlines()
ifile.close()

nefFiles = [rename_extention(f,'JPG','NEF') for f in jpgFiles]
ofile = open('names_to_delete_nef.txt', 'w')
ofile.writelines(map(lambda x: x + '\n', nefFiles))
ofile.close()


