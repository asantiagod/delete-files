
import numpy as np

def from_number(number):
    if number < 1 or number > 9999:
        return ''
    return 'DSC_' + str(number).zfill(4) +'.JPG'

def from_list(numbers):
    names = [from_number(n) for n in numbers]
    return names

def from_file(file, output_file=None):
    numbers = np.loadtxt(file,dtype=int)
    names = from_list(numbers)
    
    if output_file is None:
        return names
    else:
        wf = open(output_file, 'w')
        wf.writelines(map(lambda x: x + '\n', names))
	
